FROM alpine:3.10
RUN apk add --no-cache python3-dev build-base libffi-dev && apk add --no-cache py3-pip
WORKDIR /dir
COPY . /dir
RUN pip3 --no-cache-dir install -r requirements.txt
CMD ["python3", "src/app.py"]

